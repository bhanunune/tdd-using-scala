package com.bhanu.tdd

import org.scalatest.FlatSpec

class HelloSpecWithFlatSpec extends FlatSpec {

  "calling Hello greet" should "return Hello World" in {

    assert(Hello().greet == "Hello World")

  }
}
