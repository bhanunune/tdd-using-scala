package com.bhanu.tdd

import org.scalatest.{FeatureSpec, GivenWhenThen}
class HelloSpecWithFeatureSpec extends FeatureSpec with GivenWhenThen {

  info("As a Product Owner")
  info("I am expecting Hello World Greeting")
  feature("Greeting") {
    scenario("User expects greeting") {
      Given("Hello is Instantiated")
      val helloTest = Hello()
      When("greet method is called")
      val greetToUser = helloTest.greet
      Then("we get Hello World")
      assert(greetToUser == "Hello World")
    }
  }
}
